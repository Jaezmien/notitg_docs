ActorSound
==========

|since_itg|

.. contents:: :local:

Description
-----------

An actor that plays sounds

XML attributes
--------------

**File**

A filepath to the initial sound file to load

API reference
-------------

.. lua:autoclass:: ActorSound
