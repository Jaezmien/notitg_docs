HighScore
=========

|since_itg|

.. contents:: :local:

Description
-----------

Represents a score after completing a step

API reference
-------------

.. lua:autoclass:: HighScore
