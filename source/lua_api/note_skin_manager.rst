NoteSkinManager
===============

|since_itg|

.. contents:: :local:

Description
-----------

The note skin manager, accessible from the global singleton :lua:attr:`NOTESKIN <global.global.NOTESKIN>`

API reference
-------------

.. lua:autoclass:: NoteSkinManager
