Modifiers
=========
|since_itg|

.. contents:: :local:

This list is complete (as of |notitg_v4_0_1|), but sorely lacking in descriptions/images. Want to help contribute to
these docs? The source is available at https://gitlab.com/CraftedCart/notitg_docs

All # are ints, example: spline#rotx# can be spline1rotx3

All % are floats, example: %x can be 1.5x

<noteskin>
----------
where ``<noteskin>`` is the name of a note skin

|since_itg|

Sets the player's noteskin (eg: ``metal``)

This does not seem to work from :lua:meth:`GameState.ApplyModifiers` - use :lua:meth:`GameState.LaunchAttack`, or a
``#MODS``/``#ATTACKS`` section in your .sm file.

%x
--------
|since_itg|

Sets the scroll speed to use XMod at a given speed

With no other modifiers, 1x causes arrows to scroll up 1/10th of the screen (the size of the arrow sprite) per beat.

AddScore
--------
|since_itg|

Alternate
---------
|since_itg|

(For more complicated mods, consider using column specific Reverse instead)

Reverses the second and forth columns

https://fms-cat.com/flip-invert/

ApproachType
------------

ArrowCull
---------

ArrowPath
---------
|column_specific_available|

Shows lines for the path arrows take to the receptors

ArrowPathDiffuse|%|%|%
^^^^^^^^^^^^^^^^^^^^^^

ArrowPathDrawDistance
^^^^^^^^^^^^^^^^^^^^^

ArrowPathDrawDistanceBack
^^^^^^^^^^^^^^^^^^^^^^^^^

ArrowPathDrawDistanceFront
^^^^^^^^^^^^^^^^^^^^^^^^^^

ArrowPathDrawSize
^^^^^^^^^^^^^^^^^

ArrowPathDrawSizeBack
^^^^^^^^^^^^^^^^^^^^^

ArrowPathDrawSizeFront
^^^^^^^^^^^^^^^^^^^^^^

ArrowPathFadeBottom|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^

ArrowPathFadeBottomOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ArrowPathFadeTop|%|%|%
^^^^^^^^^^^^^^^^^^^^^^

ArrowPathFadeTopOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

ArrowPathGirth
^^^^^^^^^^^^^^

ArrowPathGrain
^^^^^^^^^^^^^^

ArrowPathGranulate
^^^^^^^^^^^^^^^^^^

ArrowPathSize
^^^^^^^^^^^^^

ArrowPathWidth
^^^^^^^^^^^^^^

Asymptote
---------

AsymptoteOffset
^^^^^^^^^^^^^^^

AsymptoteScale
^^^^^^^^^^^^^^

AsymptoteSize
^^^^^^^^^^^^^

Attenuate
---------

AttenuateOffset
^^^^^^^^^^^^^^^

AttenuateX
----------

AttenuateXOffset
^^^^^^^^^^^^^^^^

AttenuateZ
----------

AttenuateZOffset
^^^^^^^^^^^^^^^^

AverageScore
------------
|since_itg|

Beat
----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

Makes the arrows/receptors shake on beats

BeatCap
^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

BeatMult
^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

BeatOffset
^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

BeatPeriod
^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

BeatSize
^^^^^^^^

BeatY
-----

BeatCapY
^^^^^^^^

BeatYMult
^^^^^^^^^

BeatYOffset
^^^^^^^^^^^

BeatYPeriod
^^^^^^^^^^^

BeatYSize
^^^^^^^^^

BeatZ
-----

BeatCapZ
^^^^^^^^

BeatZMult
^^^^^^^^^

BeatZOffset
^^^^^^^^^^^

BeatZPeriod
^^^^^^^^^^^

BeatZSize
^^^^^^^^^

Big
---
|since_itg|

Blind
-----
|since_itg|

Warning: This mod will cause all noteflashes to appear as though they were fantastics.  Reccomend hiding Judgements with actor:hidden()

Hides the judgements, combo and causes all noteflashes to appear fantastic, removing all player feedback.

Blink
-----
|since_itg|

Arrows flash between visible and hidden

BlinkBlue / BlinkB
------------------

BlinkGreen / BlinkG
-------------------

BlinkRed / BlinkR
-----------------

BMRize
------
|since_itg|

Boomerang
---------
|since_itg|

Boost
-----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

Arrows accelerate as they approach the receptors

Bounce
------

BounceOffset
^^^^^^^^^^^^

BouncePeriod
^^^^^^^^^^^^

BounceZ
-------

BounceZOffset
^^^^^^^^^^^^^

BounceZPeriod
^^^^^^^^^^^^^

Brake / Land
------------
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

Arrows slow down as they approach the receptors

Bumpy / BumpyZ
--------------
|column_specific_available|

|since_itg|

The notes move forward and backwards along the depth axis following a sine wave.

This also enables depth testing.

BumpyOffset / BumpyZOffset
^^^^^^^^^^^^^^^^^^^^^^^^^^
|column_specific_available|

Note that column specific BumpyOffset only modifies the corresponding column specific Bumpy, and not general Bumpy


BumpyPeriod / BumpyZPeriod
^^^^^^^^^^^^^^^^^^^^^^^^^^
|column_specific_available|

BumpySize / BumpyZSize
^^^^^^^^^^^^^^^^^^^^^^

BumpyX
------
|column_specific_available|

BumpyXOffset
^^^^^^^^^^^^
|column_specific_available|

BumpyXPeriod
^^^^^^^^^^^^
|column_specific_available|

BumpyXSize
^^^^^^^^^^

BumpyY
------
|column_specific_available|

BumpyYOffset
^^^^^^^^^^^^
|column_specific_available|

BumpyYPeriod
^^^^^^^^^^^^
|column_specific_available|

BumpyYSize
^^^^^^^^^^

C%
--------
|since_itg|

Sets the scroll speed to use CMod at a given speed

Centered
--------
|since_itg|

Centers the receptors vertically on the screen. Values greater than ``100%`` move the receptors past the screen's
center.

Centered2
---------
Centers the arrows following the arrowpaths.

ClearAll
--------
|since_itg|

Resets all mods

Also resets splines if :lua:meth:`Player.NoClearSplines` is ``false``

Confusion
---------
The notes and receptors spin

ConfusionOffset
---------------
|column_specific_available|

Offset the rotation for the Confusion mod

Mod percentage is in radians multiplied by 100

ConfusionX
----------

ConfusionXOffset
----------------
|column_specific_available|

ConfusionY
----------

ConfusionYOffset
----------------
|column_specific_available|

ConfusionZ
----------

ConfusionZOffset
----------------
|column_specific_available|

Converge
--------

CosClip
-------

CouplesMirror
-------------
|turn_mod|

CouplesSwapNotes
----------------
|turn_mod|

Cover
-----
|since_itg|

Darkens the background

Cross
-----
|since_itg|

(For more complicated mods, consider using column specific Reverse instead)

Reverses the middle two columns

https://fms-cat.com/flip-invert/

CubicX
------

CubicXOffset
^^^^^^^^^^^^

CubicY
------

CubicYOffset
^^^^^^^^^^^^

CubicZ
------

CubicZOffset
^^^^^^^^^^^^

CustomNoteFlash
---------------

Dark
----
|column_specific_available|

|since_itg|

Hides the receptors, while keeping the blue/yellow/green/etc. flashes when tapping on notes

Diffuse|%|%|%
-------------

Digital
-------

DigitalOffset
^^^^^^^^^^^^^

DigitalPeriod
^^^^^^^^^^^^^

DigitalSteps
^^^^^^^^^^^^

DigitalZ
--------

DigitalZOffset
^^^^^^^^^^^^^^

DigitalZPeriod
^^^^^^^^^^^^^^

DigitalZSteps
^^^^^^^^^^^^^

DisableMines
------------

Distant
-------
|since_itg|

A notefield perspective modifier

The notefield is tilted away from the player, at a lesser angle compared to `Space`_

Dizzy
-----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

The notes spin in their lanes along the Z axis.

DizzyHolds
----------
Makes hold heads behave like regular tap notes with mods like Dizzy or Confusion

DrawDistance
------------

DrawDistanceBack
----------------

DrawDistanceFront
-----------------

DrawSize
--------
Determines how far back the arrows start drawing

DrawSizeBack
------------

DrawSizeFront
-------------

Drunk
-----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

The notes/receptors sway left and right

DrunkOffset
^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

DrunkPeriod
^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

DrunkSize
^^^^^^^^^

DrunkSpacing
^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

DrunkSpeed
^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

DrunkY
------

DrunkYOffset
^^^^^^^^^^^^

DrunkYPeriod
^^^^^^^^^^^^

DrunkYSize
^^^^^^^^^^

DrunkYSpacing
^^^^^^^^^^^^^

DrunkYSpeed
^^^^^^^^^^^

DrunkZ
------

DrunkZOffset
^^^^^^^^^^^^

DrunkZPeriod
^^^^^^^^^^^^

DrunkZSize
^^^^^^^^^^

DrunkZSpacing
^^^^^^^^^^^^^

DrunkZSpeed
^^^^^^^^^^^

Echo
----
|since_itg|

Expand
------
|since_itg|

ExpandPeriod
^^^^^^^^^^^^

ExpandSize
^^^^^^^^^^

FadeBottom|%|%|%
----------------

FadeBottomOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^

FadeTop|%|%|%
-------------

FadeTopOffset|%|%|%
^^^^^^^^^^^^^^^^^^^

FallX|%|%|%
-----------
|column_specific_available|

FallY|%|%|%
-----------
|column_specific_available|

FallZ|%|%|%
-----------
|column_specific_available|

Flip
----
|since_itg|

Horizontally flips all four columns.

Values between ``0%`` and ``50%`` can be used to change the spacing between receptors, keeping the column order the
same;

``50%`` flip places all the four receptors on top of each other;

and values between ``50%`` and ``100%`` can be used to change the spacing between receptors, with the order being
flipped.

https://fms-cat.com/flip-invert/

Floored
-------
|since_itg|

GayHolds
--------
|column_specific_available|

The opposite of `StraightHolds`_

GlitchyTan / CoSec
------------------

GlobalModTimer / ModTimer / Timer
---------------------------------
By default, some mods (like ``Drunk``) operate on a global timer, meaning stuff can look slightly different between
plays of a chart. ``GlobalModTimer`` makes these mods use the song position instead of the global timer, so that the mod will
look the exact same each time (Eg: so ``Drunk`` will look the exact same 15 seconds into a song every time).

GlobalModTimerMult / ModTimerMult / TimerMult
---------------------------------------------

GlobalModTimerOffset / ModTimerOffset / TimerOffset
---------------------------------------------------

Grain / Granulate
-----------------

Halgun
------
Hides hold judgements. (Yeah! and NG)

Hallway
-------
|since_itg|

A notefield perspective modifier

The notefield is tilted towards the player, at a lesser angle compared to `Incoming`_

Hidden
------
|since_itg|

Flashes arrows (white by default) and hides them as they approach the receptors

HiddenOffset
^^^^^^^^^^^^
|since_itg|

HiddenBlue / HiddenB
--------------------

HiddenBlueOffset / HiddenBO
^^^^^^^^^^^^^^^^^^^^^^^^^^^

HiddenGreen / HiddenG
---------------------

HiddenGreenOffset / HiddenGO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

HiddenRed / HiddenR
-------------------

HiddenRedOffset / HiddenRO
^^^^^^^^^^^^^^^^^^^^^^^^^^

Hide
----

HideHolds
---------
|column_specific_available|

Hides the hold bit of hold notes

HideMines
---------
|column_specific_available|

Hides mines

HideNoteFlash / HideNoteFlashes
-------------------------------
|column_specific_available|

Hides the blue/yellow/green/etc. receptor flashes when arrows are hit

HideNotePress
-------------
|since_notitg_v4_2_0|

Alias of `ManualNoteFlash`

HoldCull
--------

HoldGirth
---------
|column_specific_available|

HoldStealth
-----------
|column_specific_available|

Like Steath but on hold tails.

HoldsToRolls
------------
|since_itg|

HoldTiny
--------
|column_specific_available|

Incoming
--------
|since_itg|

A notefield perspective modifier

The notefield is tilted towards the player - notes appear farther away, coming closer to the player as they approach
the receptors.

Invert
------
|since_itg|

Pulls the outer two columns to the middle, pulls the inner two columns to the outer

https://fms-cat.com/flip-invert/

Judgescale / Timing
-------------------
|since_itg|

Left
----
|since_itg|

|turn_mod|

Permutes the columns, causing the player to play the chart as though they had turned to the left.

- Left -> Down
- Down -> Right
- Up -> Left
- Right -> Up

Little
------
|since_itg|

LongBoy / LongBoys / LongHolds
------------------------------
Makes holds appear longer/shorter than they actually are

``100% LongHolds`` makes holds appear to be double their length, ``-100% LongHolds`` hides holds entirely.

M%
--
|since_itg|

Sets the scroll speed to use MMod at a given speed

ManualNoteFlash
---------------

Hides note presses

MetaDizzy
---------
|since_notitg_v4_2_0|

|metamod|

MetaFlip
--------
|since_notitg_v4_2_0|

|metamod|

MetaInvert
----------
|since_notitg_v4_2_0|

|metamod|

MetaOrient
----------
|since_notitg_v4_2_0|

|metamod|

MetaReverse
-----------
|since_notitg_v4_2_0|

|metamod|

MetaStealth
-----------
|since_notitg_v4_2_0|

|metamod|

Mines
-----
|since_itg|

MineStealth
-----------
|column_specific_available|

Like Stealth but only on mines.

Mini
----
|since_itg|

Shrinks/grows the playfield

``100% Mini`` makes the playfield half size, ``200% Mini`` makes the playfield zero sized, negative values make the
playfield larger.

Mirror
------
|since_itg|

|turn_mod|

Permutes the columns, swapping Left and Right, and Up and Down.

- Left -> Right
- Down -> Up
- Up -> Down
- Right -> Left

MoveX
-----
|column_specific_available|

Moves the playfield in the X direction. ``100% MoveX`` moves the playfield by the size of the arrow sprite (64 pixels).

MoveY
-----
|column_specific_available|

Moves the playfield in the Y direction. ``100% MoveY`` moves the playfield by the size of the arrow sprite (64 pixels).

MoveZ
-----
|column_specific_available|

Moves the playfield in the Z direction. ``100% MoveZ`` moves the playfield by the size of the arrow sprite (64 pixels).

NoFreeze
--------

NoHands
-------
|since_itg|

NoHoldJudge
-----------

NoHolds
-------
|since_itg|

Removes holds in the chart.

NoJumps
-------
|since_itg|

Removes all apperances of 2 arrows at the same time.

NoMines
-------
|since_itg|

Removes all mines

NoQuads
-------
|since_itg|

Removes all apperances of 4 arrows at the same time.

NoRolls
-------
|since_itg|

Removes all rolls in the chart.

NoStretch
---------
|since_itg|

What counts as a strech?

NoteSkew / NoteSkewX
--------------------
|column_specific_available|

Skews notes / receptors in the x axis.

NoteSkewY
---------
|column_specific_available|

Skews notes / receptors in the y axis.

NoteSkewType
------------

NoteSkin
--------
?

Orient
------
|since_notitg_v4|

Additionally rotates arrows in the direction of travel relative to "upwards".

It can also be used in percentages, to increase or decrease or even invert the effect.

For downwards scroll (e.g. with Reverse or splines), combine this mod with 314% ConfusionOffset

Since |notitg_v4_2_0|, ``Orient`` now reorients itself when when reverse and SCAR mods are enabled.  If you have used
Orient+Reverse before, setting 314% ConfusionOffset is no longer required.

OrientOffset
^^^^^^^^^^^^
|since_notitg_v4_2_0|

It changes the direction the `Orient` mod should reference

NoReorient
^^^^^^^^^^
|since_notitg_v4_2_0|

Disables the `Orient` behavior optimized for reverse and SCAR families

Overhead
--------
|since_itg|

The default perspective - notes scroll on a flat surface from bottom to top

ParabolaX
---------

ParabolaXOffset
^^^^^^^^^^^^^^^

ParabolaY
---------

ParabolaYOffset
^^^^^^^^^^^^^^^

ParabolaZ
---------

ParabolaZOffset
^^^^^^^^^^^^^^^

Passmark
--------
|since_itg|

Planted
-------
|since_itg|

Pulse
-----

PulseInner
----------

PulseOuter
----------

PulseOffset
-----------

PulsePeriod
-----------

Quick
-----
|since_itg|

Random
------
|since_itg|

Randomize
---------
|since_notitg_v4_2_0|

Randomize shuffles the notes while they are within the “invisibe” region created by Vanish.

RandomizeOffset
^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

Adjusts the location where `Randomize` takes effect.

RandomizeMirror
---------------

RandomizeOffset
---------------

RandomSpeed
-----------
|since_itg|

RandomVanish
------------
|since_itg|

A combination of the `Randomize` and `Vanish` mods.

RandomVanishOffset
^^^^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

Controls both `RandomizeOffset` and `VanishOffset`

ReceptorZBuffer
---------------
|column_specific_available|

Reverse
-------
|column_specific_available|

Pulls all four receptors to the bottom of the playfield and makes the arrows come down from the top

https://fms-cat.com/flip-invert/

ReverseType
^^^^^^^^^^^
Allows reverse to go beyond 100% values.  (without ReverseType, 130% reverse will look identical to 70% reverse)

Right
-----
|since_itg|

|turn_mod|

Permutes the columns, causing the player to play the chart as though they had turned to the right.

- Left -> Up
- Down -> Left
- Up -> Right
- Right -> Down

Roll
----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

The notes spin in their lanes around the X axis.

RotationX
---------
Rotates the playfield around the x axis.

Units are in degrees.

RotationY
---------
Rotates the playfield around the y axis.

Units are in degrees.

RotationZ
---------
Rotates the playfield around the z axis.

Units are in degrees.

Sawtooth
--------
|column_specific_available_since_notitg_v4_2_0|

SawtoothOffset
^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

SawtoothPeriod
^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

SawtoothSize
^^^^^^^^^^^^

SawtoothZ
---------

SawtoothZOffset
^^^^^^^^^^^^^^^

SawtoothZPeriod
^^^^^^^^^^^^^^^

SawtoothZSize
^^^^^^^^^^^^^

ScreenFilter
------------

ScrollSpeedMult
---------------
|since_notitg_v4_2_0|

|column_specific_available|

Multipies the speed mod of a single column.

Default value is 100%. 50% is half-speed.

ShrinkLinear
------------

ShrinkLinearX
-------------

ShrinkLinearY
-------------

ShrinkLinearZ
-------------

ShrinkMult
----------

ShrinkMultX
-----------

ShrinkMultY
-----------

ShrinkMultZ
-----------

Shuffle
-------
|since_itg|

|turn_mod|

SinClip
-------

SkewType
--------

SkewX
-----

SkewY
-----

Skippy
------
|since_itg|

SmartBlender
------------
|turn_mod|

Randomly changes chart notedata, in a way that does not cause doublesteps and preserves some chart properties, such as timing of jacks and drills.

SmartBlender will never output a file with crossovers or footswitches.

SoftShuffle
-----------
|turn_mod|

Randomly applies SwapLeftRight and SwapUpDown, with 4 possible resulting charts.

Space
-----
|since_itg|

A notefield perspective modifier

The notefield is tilted away from the player.

SpiralHolds / HoldType
----------------------
A different hold renderer (very noticable on the Y axis)

SpiralX
-------

SpiralXOffset
^^^^^^^^^^^^^

SpiralXPeriod
^^^^^^^^^^^^^

SpiralY
-------

SpiralYOffset
^^^^^^^^^^^^^

SpiralYPeriod
^^^^^^^^^^^^^

SpiralZ
-------

SpiralZOffset
^^^^^^^^^^^^^

SpiralZPeriod
^^^^^^^^^^^^^

Spline#RotX#
------------

Spline#RotXOffset#
------------------

Spline#RotXReset
----------------

Spline#RotY#
------------

Spline#RotYOffset#
------------------

Spline#RotYReset
----------------

Spline#RotZ#
------------

Spline#RotZOffset#
------------------

Spline#RotZReset
----------------

Spline#Size#
------------

Spline#SizeOffset#
------------------

Spline#SizeReset
----------------

Spline#Skew#
------------

Spline#SkewOffset#
------------------

Spline#SkewReset
----------------

Spline#Stealth#
---------------

Spline#StealthOffset#
---------------------

Spline#StealthReset
-------------------

Spline#Tiny#
------------

Spline#TinyOffset#
------------------

Spline#TinyReset
----------------

Spline#X#
---------

Spline#XOffset#
---------------

Spline#XReset
-------------

Spline#Y#
---------

Spline#YOffset#
---------------

Spline#YReset
-------------

Spline#Z#
---------

Spline#ZOffset#
---------------

Spline#Zoom#
------------

Spline#ZoomOffset#
------------------

Spline#ZoomReset
----------------

Spline#ZReset
-------------

SplineRotX#
-----------

SplineRotXOffset#
-----------------

SplineRotXReset
---------------

SplineRotXType
--------------

SplineRotY#
-----------

SplineRotYOffset#
-----------------

SplineRotYReset
---------------

SplineRotYType
--------------

SplineRotZ#
-----------

SplineRotZOffset#
-----------------

SplineRotZReset
---------------

SplineRotZType
--------------

SplineSize#
-----------

SplineSizeOffset#
-----------------

SplineSizeReset
---------------

SplineSizeType
--------------

SplineSkew#
-----------

SplineSkewOffset#
-----------------

SplineSkewReset
---------------

SplineSkewType
--------------

SplineStealth#
--------------

SplineStealthOffset#
--------------------

SplineStealthReset
------------------

SplineStealthType
-----------------

SplineTiny#
-----------

SplineTinyOffset#
-----------------

SplineTinyReset
---------------

SplineTinyType
--------------

SplineX#
--------

SplineXOffset#
--------------

SplineXReset
------------

SplineXType
-----------

SplineY#
--------

SplineYOffset#
--------------

SplineYReset
------------

SplineYType
-----------

SplineZ#
--------

SplineZOffset#
--------------

SplineZoom#
-----------

SplineZoomOffset#
-----------------

SplineZoomReset
---------------

SplineZoomType
--------------

SplineZReset
------------

SplineZType
-----------

Split
-----
|since_itg|

(For more complicated mods, consider using column specific Reverse instead)

Reverses the rightmost two columns

https://fms-cat.com/flip-invert/

SpookyShuffle
-------------
|turn_mod|

Square
------

SquareOffset
^^^^^^^^^^^^

SquarePeriod
^^^^^^^^^^^^

SquareZ
-------

SquareZOffset
^^^^^^^^^^^^^

SquareZPeriod
^^^^^^^^^^^^^

Stealth
-------
|column_specific_available|

|since_itg|

Hides the arrows. 50% turns the arrows completely white. 100% makes the arrows completely hidden.

Arrows not hit are still visible after passing the receptors (see `StealthPastReceptors`_)

StealthBlue / StealthB
----------------------

StealthGreen / StealthG
-----------------------

StealthRed / StealthR
---------------------

StealthGlow|%|%|%
-----------------

|column_specific_available|

StealthGlowBlue / StealthGB
---------------------------

StealthGlowGreen / StealthGG
----------------------------

StealthGlowRed / StealthGR
--------------------------

StealthPastReceptors
--------------------
If you have Stealth enabled (or a stealth spline), by default arrows that aren't hit will appear again once they fly
past the receptors - this makes them not do that

StealthType
-----------

Stomp
-----

StraightHolds
-------------
|column_specific_available|

SubtractScore
-------------
|since_itg|

Sudden
------
|since_itg|

Makes arrows appear on the notefield closer to the receptors than usual

SuddenOffset
^^^^^^^^^^^^
|since_itg|

Toy around with different percentage values to adjust how far away arrows appear with the Sudden mod

Stealth splines are easier if you want to be exact

SuddenBlue / SuddenB
--------------------

SuddenBlueOffset / SuddenBO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SuddenGreen / SuddenG
---------------------

SuddenGreenOffset / SuddenGO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

SuddenRed / SuddenR
-------------------

SuddenRedOffset / SuddenRO
^^^^^^^^^^^^^^^^^^^^^^^^^^

SuperShuffle
------------
|since_itg|

|turn_mod|

SwapLeftRight
-------------
|turn_mod|

Permutes the columns, swapping the left and right arrows.

- Left -> Right
- Down -> Down
- Up -> Up
- Right -> Left

SwapSides
---------
|turn_mod|

SwapUpDown
----------
|turn_mod|

Permutes the columns, swapping the up and down arrows.

- Left -> Left
- Down -> Up
- Up -> Down
- Right -> Right

TanBumpy / TanBumpyZ#
---------------------
|column_specific_available|

TanBumpyOffset
^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyPeriod
^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpySize
^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyX
---------
|column_specific_available|

TanBumpyXOffset
^^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyXPeriod
^^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyXSize
^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyY
---------
|column_specific_available|

TanBumpyYOffset
^^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyYPeriod
^^^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyYSize
^^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

TanBumpyZ
---------
|since_notitg_v4_2_0|

|column_specific_available|

TanBumpyZOffset
^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

|column_specific_available_since_notitg_v4_2_0|

TanBumpyZPeriod
^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

|column_specific_available_since_notitg_v4_2_0|

TanBumpyZSize
^^^^^^^^^^^^^
|since_notitg_v4_2_0|

|column_specific_available_since_notitg_v4_2_0|

TanClip
-------

TanDigital
----------

TanDigitalOffset
^^^^^^^^^^^^^^^^

TanDigitalPeriod
^^^^^^^^^^^^^^^^

TanDigitalSteps
^^^^^^^^^^^^^^^

TanDigitalZ
-----------

TanDigitalZOffset
^^^^^^^^^^^^^^^^^

TanDigitalZPeriod
^^^^^^^^^^^^^^^^^

TanDigitalZSteps
^^^^^^^^^^^^^^^^

TanDrunk
--------

TanDrunkOffset
^^^^^^^^^^^^^^

TanDrunkPeriod
^^^^^^^^^^^^^^

TanDrunkSize
^^^^^^^^^^^^

TanDrunkSpacing
^^^^^^^^^^^^^^^

TanDrunkSpeed
^^^^^^^^^^^^^

TanDrunkY
---------

TanDrunkYOffset
^^^^^^^^^^^^^^^

TanDrunkYPeriod
^^^^^^^^^^^^^^^

TanDrunkYSize
^^^^^^^^^^^^^

TanDrunkYSpacing
^^^^^^^^^^^^^^^^

TanDrunkYSpeed
^^^^^^^^^^^^^^

TanDrunkZ
---------

TanDrunkZOffset
^^^^^^^^^^^^^^^

TanDrunkZPeriod
^^^^^^^^^^^^^^^

TanDrunkZSize
^^^^^^^^^^^^^

TanDrunkZSpacing
^^^^^^^^^^^^^^^^

TanDrunkZSpeed
^^^^^^^^^^^^^^

TanExpand
---------

TanExpandPeriod
^^^^^^^^^^^^^^^

TanExpandSize
^^^^^^^^^^^^^

TanPulse
--------

TanPulseInner
-------------

TanPulseOuter
-------------

TanPulseOffset
--------------

TanPulsePeriod
--------------

TanTipsy
--------

TanTipsyOffset
^^^^^^^^^^^^^^

TanTipsySpacing
^^^^^^^^^^^^^^^

TanTipsySpeed
^^^^^^^^^^^^^

TanTornado
----------

TanTornadoOffset
^^^^^^^^^^^^^^^^

TanTornadoPeriod
^^^^^^^^^^^^^^^^

TanTornadoZ
-----------

TanTornadoZOffset
^^^^^^^^^^^^^^^^^

TanTornadoZPeriod
^^^^^^^^^^^^^^^^^

TextureFilterOff
----------------
|column_specific_available|

Tiny
----
|column_specific_available|

|since_notitg_unk|

Reduces the size of the arrows, without changing their position relative to each other like mini does.  200% will cause the arrows to dissapear entirely.

TinyX
-----
|column_specific_available|

Reduces the size of the arrows in the x direction.

TinyY
-----
|column_specific_available|

Reduces the size of the arrows in the x direction.

TinyZ
-----
|column_specific_available|

Reduces the size of the arrows in the x direction.

Tipsy
-----
|since_itg|

TipsyOffset
^^^^^^^^^^^

TipsySpacing
^^^^^^^^^^^^

TipsySpeed
^^^^^^^^^^

Tornado
-------
|since_itg|

The notes fly in from the left, to the right, as they move towards the receptors

TornadoOffset
^^^^^^^^^^^^^

TornadoPeriod
^^^^^^^^^^^^^

TornadoZ
--------

TornadoZOffset
^^^^^^^^^^^^^^

TornadoZPeriod
^^^^^^^^^^^^^^

Turn
----
|since_itg|

Twirl
-----
|since_itg|

|column_specific_available_since_notitg_v4_2_0|

The notes and their holds spin in 3D (around the Y/up axis)

This creates a nice twisting appearance for the holds

Twister
-------
|since_itg|

Ultraman
--------

Vanish
------
|since_notitg_v4_2_0|

Vanish makes the arrows disappear for a bit at a spot in the middle of the screen.

VanishOffset
^^^^^^^^^^^^
|since_notitg_v4_2_0|

Adjusts the location where `Vanish` takes effect.

VanishSize
^^^^^^^^^^
|since_notitg_v4_2_0|

Wave
----
|since_itg|

|column_specific_available|

WaveOffset
^^^^^^^^^^
|column_specific_available|

WavePeriod
^^^^^^^^^^
|column_specific_available|

WaveSize
^^^^^^^^

Wide
----
|since_itg|

WireFrame
---------
|column_specific_available|

WireFrameGirth
^^^^^^^^^^^^^^
|column_specific_available|

WireFrameWidth
^^^^^^^^^^^^^^
|column_specific_available|

X
-
Moves the playfield in the x direction.

Y
-
Moves the playfield in the y direction.

Z
-
Moves the playfield in the z direction.

ZBuffer
-------
Makes arrows/holds read from and write to the depth/Z buffer

Zigzag
------
|column_specific_available_since_notitg_v4_2_0|

ZigzagOffset
^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

ZigzagPeriod
^^^^^^^^^^^^
|column_specific_available_since_notitg_v4_2_0|

ZigzagSize
^^^^^^^^^^

ZigzagZ
-------

ZigzagZOffset
^^^^^^^^^^^^^

ZigzagZPeriod
^^^^^^^^^^^^^

ZigzagZSize
^^^^^^^^^^^

Zoom
----

ZoomX
-----

ZoomY
-----

ZoomZ
-----

ZTest
-----

.. |column_specific_available| replace:: |receptor_16| Column-specific variant available (add the column number, 0
   indexed, to the end of the mod name)

.. |column_specific_available_since_notitg_v4_2_0| replace:: |receptor_16| Column-specific variant available since
   |notitg_v4_2_0| (add the column number, 0 indexed, to the end of the mod name)

.. |turn_mod| replace:: (This mod is a Turn mod, and directly affects the player's notedata. It cannot be applied mid-file.)

.. |metamod| replace:: (This mod is a metamod, cannot be applied by a simfile, and will disqualify your score.)
